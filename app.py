import requests
import time
from bs4 import BeautifulSoup

from apistar import App, Include, Route
from apistar.docs import docs_routes
from apistar.statics import static_routes

def prepare_url():
    """
    Return URL with current date in it
    """
    date_format = time.strftime('%d%m')
    url = "http://gostilna-ponvica.si/sl/dnevna-ponudba-%s.html" % date_format

    return url

def open_frontpage():
    url = "http://www.gostilna-ponvica.si/sl/"
    response = requests.get(url)
    body = response.text

    soup = BeautifulSoup(body, 'html.parser')

    for a in soup.find_all('a', href=True):
        if 'http://www.gostilna-ponvica.si/sl/news/malice-in-kosila/malice-' in a['href']:
            return a['href']
    return None


def fetch_menu(url):
    """
    Return HTML menu list
    """
    # TODO Add try/except because this will not work on weekends
    # TODO Add identifying user agent
    response = requests.get(url)
    body = response.text

    soup = BeautifulSoup(body, 'html.parser')
    menu_html = soup.find('div', {'class': 'novice_container'})
    menu_html = menu_html.find_all('p')
    menu_html = " ".join([str(x) for x in menu_html])

    return menu_html


def menu():
    """
    Return Ponvica's daily menu

    1) Prepare URL to reflect each working day
    2) Make a request to this URL and return the list of options to eat
    3) Use this endpoint in Hipchat integrations with a custom / command

    Bon apetit!
    """
    # Prepare date to be put in the menu URL
    url = open_frontpage()

    menu_html = fetch_menu(url)

    return {
        'message': menu_html,
        'color': 'green',
        'notify': True,
        'message_format': 'html'
    }


routes = [
    Route('/', 'POST', menu),
    Include('/docs', docs_routes),
    Include('/static', static_routes)
]

app = App(routes=routes)
